#!/usr/bin/python
# -*- coding: utf-8 -*-
# $Id: bot_dobby.py,v 0.7c 20150522-0910 by Evgeniy Burmistrov, [Original script taken from "bot.py,v 1.2 2006/10/06 12:30:42" ]
# Changes: " optimized 'GLs','botpsqlHandler','botHandler','botcmdHandler' "|" something " " added psql SELECT for mytasksHandler"
import sys, xmpp, re, subprocess, os
reload(sys);sys.setdefaultencoding("utf-8")
global GLdirLOG,GLs;args,command,user = '','',''
GLdirLOG = ['/var/log/dobby','/var/log/dobby/_cmd.log','/var/log/dobby/_psql.log','/var/log/dobby/_notify','/var/log/dobby/_tasklist.log','','','','/var/log/dobby/_debug.log','/tmp/msg.tmp']
GLs = ['echo $(date +%Y%m%d-%H%M%S):',"'(from="+str(user)+")[cmd="+str(command)+" "+str(args)+"]'","echo '--------------------",'']
GLpsql = ["export PGPASSFILE=/tmp/.pgpass;echo '127.0.0.1:*:*:test_user:123' > $PGPASSFILE;chmod 0600 $PGPASSFILE;psql -h 127.0.0.1 -U test_user test_database -c",'']
commands={}
i18n={'ru':{},'en':{}}
########################### user handlers start ##################################
def tmpGUI(): return "%s"%(open(GLdirLOG[9]).read())
def helpHandler(user,command,args,mess):
    i18n['en']['varTXT']="This is jabber bot 'Dobby'.\nbot command example:\nbot to=test1@10.0.11.65 from=test2@10.011.65 Msg=Yahhhr LADS!\n\nAvailable bot commands: \n%s"
    commands.keys().sort()
    return "varTXT",', '.join(commands.keys())
def botinfoHandler(user,command,args,mess):
    if mess.getFrom() == 'test0@10.0.11.65/PC0998':    return "FROM: %s\nI am: ' %s ', my home is: ' %s ' server and here is my secret: ' %s '" %(mess.getFrom(),jid.getNode(),jid.getDomain(),password)
    else: return "%s"%(open("/home/user/.scripts/git/repo/ejabberd_bot-dobby/ascii/ascii_dobby-not").read())
def botcopyHandler(user,command,args,mess):	return "You are: ' %s ', and you have wrote me: [%s]"%(mess.getFrom(),mess.getBody())
def botdebugHandler(user,command,args,mess):	return "user: ' %s ', command: ' %s ', args: ' %s ', mess: ' %s '"%(user,command,args,mess)
def mytasksHandler(user,command,args,mess):
    os.system(""+str(GLpsql[0])+" 'SELECT tasklist FROM users;'| head -n -2| tail -n +3 > '"+str(GLdirLOG[9])+"'")
#    os.system("(echo 'Your last tasks: '; head -10 /home/user/.scripts/tasklist/$(expr "+str(user)+" : '\(.[a-zA-Z0-9]*@[a-zA-Z0-9.]*\)')/tasks.lst) > '"+str(GLdirLOG[9])+"'")
    os.system(""+str(GLs[0])+" "+str(GLs[1])+" >> "+str(GLdirLOG[4])+";"+str(args)+" >> "+str(GLdirLOG[4])+";"+str(GLs[2])+"9' >> "+str(GLdirLOG[4]))
    return tmpGUI()
def botHandler(user,command,args,mess):
    varTo,varFrom,m1,m2,m3 = '','',re.search('(?<=to=)(.*?) ', args),re.search('(?<=from=)(.*?) ', args),re.search('(?<=Msg=)(.*)', args)
    varTo, varFrom, varMsg = m1.group(1) if m1 else '', m2.group(1) if m2 else '', m3.group(1) if m3 else ''
    if varTo != '' and varMsg != '':
      conn.send(xmpp.Message(varTo,varMsg))
      os.system(""+str(GLs[0])+" "+str(GLs[1])+" >> "+str(GLdirLOG[3])+";"+str(varMsg)+" >> "+str(GLdirLOG[3])+";"+str(GLs[2])+"2' >> "+str(GLdirLOG[3]))
def botpsqlHandler(user,command,args,mess):
    if "re.search('(?<=psql)(.*)', args)":
      os.system("sudo -u postgres bash -c '"+str(args)+"' > '"+str(GLdirLOG[9])+"'")
      os.system(""+str(GLs[0])+" "+str(GLs[1])+" >> "+str(GLdirLOG[2])+"; sudo -u postgres bash -c '"+str(args)+"' >> "+str(GLdirLOG[2])+";"+str(GLs[2])+"1' >> "+str(GLdirLOG[2]))
      return tmpGUI()
    else: os.system(""+str(GLs[0])+" "+str(GLs[1])+" >> "+str(GLdirLOG[2])+";"+str(varMsg)+" >> "+str(GLdirLOG[2])+";"+str(GLs[2])+"' >> "+str(GLdirLOG[2]))
def botcmdHandler(user,command,args,mess):
    os.system(str(args)+" > '"+(GLdirLOG[9])+"'")
    os.system(""+str(GLs[0])+" "+str(GLs[1])+" >> "+str(GLdirLOG[1])+";"+str(args)+" >> "+str(GLdirLOG[1])+";"+str(GLs[2])+"0' >> "+str(GLdirLOG[1]))
    return tmpGUI()
########################### user handlers stop ###################################
############################ bot logic start #####################################
i18n['en']["UNKNOWN COMMAND"]='Unknown command "%s". Try "help"'
i18n['en']["UNKNOWN USER"]="I do not know you. Register first."
def messageCB(conn,mess):
    text,user=mess.getBody(),mess.getFrom()
    user.lang='en'      # dup
    if text.find(' ')+1: command,args=text.split(' ',1)
    else: command,args=text,''
    global GLs;GLs = ['echo $(date +%Y%m%d-%H%M%S):',"'(from="+str(user)+")[cmd="+str(command)+" "+str(args)+"]'","echo '--------------------"]
    cmd=command.lower()

    if commands.has_key(cmd): reply=commands[cmd](user,command,args,mess)
    else: return "%s"%(os.system("if [ -f '"+str(GLdirLOG[9])+"' ];then rm '"+str(GLdirLOG[9])+"';fi"))

    if type(reply)==type(()):
        key,args=reply
        if i18n[user.lang].has_key(key): pat=i18n[user.lang][key]
        elif i18n['en'].has_key(key): pat=i18n['en'][key]
        else: pat="%s"
        if type(pat)==type(''): reply=pat%args
        else: reply=pat(**args)
    else:
        try: reply=i18n[user.lang][reply]
        except KeyError:
            try: reply=i18n['en'][reply]
            except KeyError: pass
    if reply: conn.send(xmpp.Message(mess.getFrom(),reply))
    os.system("if [ -f '"+str(GLdirLOG[9])+"' ];then rm '"+str(GLdirLOG[9])+"';fi")

for i in globals().keys():
    if i[-7:]=='Handler' and i[:-7].lower()==i[:-7]: commands[i[:-7]]=globals()[i]
############################# bot logic stop #####################################
def StepOn(conn):
    try:
        conn.Process(1)
    except KeyboardInterrupt: return 0
    return 1
def GoOn(conn):
    while StepOn(conn): pass

if len(sys.argv)<100:
    jid,password=xmpp.JID('dobby@10.0.11.65'),'zHsQRGBVehr5IFKE7zaVHekBlVKpR4ab'
    user,server=jid.getNode(),jid.getDomain()
    conn=xmpp.Client(server)#,debug=[])
    conres=conn.connect()
    if not conres:
        print "Unable to connect to server %s!"%server
        sys.exit(1)
    if conres<>'tls':
        print "Warning: unable to estabilish secure connection - TLS failed!"
    authres=conn.auth(user,password)
    if not authres:
        print "Unable to authorize on %s - check login/password."%server
        sys.exit(1)
    if authres<>'sasl':
        print "Warning: unable to perform SASL auth os %s. Old authentication method used!"%server
    conn.RegisterHandler('message',messageCB)
    conn.sendInitPresence()
    print "Bot [Dobby] started."
    os.system("if ! [ -d '"+str(GLdirLOG[0])+"' ];then mkdir -p '"+str(GLdirLOG[0])+"';fi")

    GoOn(conn)
