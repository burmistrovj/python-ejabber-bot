## ejabberd_bot-dobby
# Bot "Dobby" for ejabberd server #


**Usage:**

-run as python script in background [CRON available]

-Dobby receives commands and executes them on jabber server side, also logs them default to /var/log/dobby


**Files:**

bot_dobby.py			-	python bot that listens for incoming commands and executes them if they match certain conditions

	commands:

	  bot			-	sends message to another user through 'dobby' [ Example: bot to=someuser@ejabberserver.net Msg=SomeLongTextMessage(MUST BE LAST) ]

	  botpsql		-	sends command to postgresql from 'postgres' user using psql command [ Example: botpsql psql -l ]

	  botcmd		-	(deprecated)sends command in linux through dobby and receive output

	  bottasks		-	parse user that have sended command to dobby and returns the %JID@EJABBER%tasks.lst file last 10 lines, removes %RESOURCE% during parsing [ Example: bottasks ]

sieve/				-	Used for scripts that are placed in dovecot sieve plugin for execution

sieve/bot_dobby.sieve		-	sieve script that filters incoming emails 'subject' and sends it via sendxmpp client to ejabberd user 'dobby@ejabberserver.net' if it matches conditions, chmod 600 & sieve trusted user

sieve/.sendxmpprc		-	sendxmpp config file, used to connect with 'dobby', chmod 600 & sieve trusted user [apt-get install sendxmpp]

sieve/custom_sendxmpp.sh	-	sendxmpp executive script by sieve ExtProgram 'vnd.dovecot.execute', receives filtered data and exectes from 'bot_dobby.sieve', chmod 700 & sieve trusted user, depends on .sendxmpprc


**Adds:**

All bot commands output are stored in /var/log/dobby, be sure to set rights permission for the folder to chmod 660 and user group that are related to running 'bot_dobby.py' script


**CRONTAB:**

Perform 'bot_dobby.py' script check that it is running as process, if not, start it. This will make sure that bot will stay online during crashes if any occur

* * * * *      root    if ps aux | grep "bot_dobby.py"|grep -v grep > /dev/null;then echo "">/dev/null 2>&1;else "$(/home/user/.scripts/git/repo/ejabberd_bot-dobby/bot_dobby.py &)";fi



**NOTIFICATION FLOW:**

[Mail client] -> [Mail Send with "#THEME"] -> [Dovecot SRV] -> [Sieve] -> [Sieve: ExtPrograms plugin] -> [Sieve: Filter script] ->

-> [ Filtered message send via sendxmpp script ] -> [ Ejabberd bot user "script.py" ] -> [Jabber client]


    +----------+    Mail Send    +-----------+    +------------------------------+   +---------+ Filtered message  +-----------+   +--------+
    |          |    with #THEME  |           |    |         |    |               |   |         | send via sendxmpp | Ejabberd  |   |        |
    |   mail   ------------------>   Dovecot +----+  Sieve  |    |  ExtPrograms  |   |  filter |      script       | bot user  |   | jabber |
    |  Client  |                 |    SRV    |    |         |    |    plugin     ---->  script --------------------> script.py ----> client |
    |          |                 |           |    |         |    |               |   |         |                   |           |   |        |
    +----------+                 +-----------+    +---------+    +---------------+   +---------+                   +-----------+   +--------+